<?php

	namespace App\Helpers;
	use Illuminate\Support\Facades\Storage;
	use Image;

	
	class Helper{

	    public function imageupload($request)
	    {
	        //image name with Extension
            $imagewithextension = $request->getClientOriginalName();
            //image name without Extension
            $imagename = pathinfo($imagewithextension, PATHINFO_FILENAME);
			//get file extension
            $extension = $request->getClientOriginalExtension();
            $image = $imagename.'_'.uniqid().'.'.$extension;

            Storage::put('public/images/'. $image, fopen($request, 'r+'));
            Storage::put('public/images/thumbnail/'. $image, fopen($request, 'r+'));
            $thumbnailpath = public_path('storage/images/thumbnail/'.$image);
            $img = Image::make($thumbnailpath)->resize(100, 100)->save($thumbnailpath);

	        return $image;
	    }
	}