<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostStoreRequest;
use App\Post;
use App\Comment;
use App\User;

class CommentController extends Controller
{
	public function index(){
		
	}

	public function store(Request $request, $id){
		$this->validate($request,[
			'comment'=>'required',
		]);
		$post = Post::find($id);
		$user_id = $post->user_id;
		$comment = new Comment;
		$comment->post_id = $id;
		$comment->user_id = $user_id;
		$comment->content = $request->comment;
		$comment->status = true;
		$result = $comment->save();
		return redirect()->route('/')->with('message', 'Invalid credentials');
	}
}
