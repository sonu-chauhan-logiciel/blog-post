<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use App\Post;

class ImageController extends Controller
{
    //

    public function index($id){
    	$post = Post::findOrFail($id);
    	return view('posts.postImage', [
    		'post' => $post
    	]);
    }

    public function store(Request $request, $id){
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        if ($request->hasfile('image')) {

            //image name with Extension
            $imagewithextension = $request->file('image')->getClientOriginalName();
            //image name without Extension
            $imagename = pathinfo($imagewithextension, PATHINFO_FILENAME);
            //get file extension
            $extension = $request->file('image')->getClientOriginalExtension();
            $name = $imagename.'_'.uniqid().'.'.$extension;
            $request->file('image')->move(public_path().'/blog_images/', $name);    
        }
        $image = new Image;
        $image->post_id = $id;
        $image->img_name = $name;   
        $image->status = $request->status;   
        $image->save();
       return redirect()->back()->with('message', 'Image Uloaded successfully');

    	/*if($request->hasfile('images')){
    		foreach($request->file('images') as $image)
            {
                $imageWithExtension=$image->getClientOriginalName(); 
                //image name without Extension
                $imagename = pathinfo($imageWithExtension, PATHINFO_FILENAME);
                $extension = $image->getClientOriginalExtension();
                $name = $imagename.'_'.uniqid().'.'.$extension;
                $image->move(public_path().'/blog_images/', $name);
                $image = new Image;
                $image->post_id = $id;
                $image->img_name = $name;   
                $image->status = $request->status;   
                $image->save();
            }
        }
        return redirect()->route('posts.index')->with('message', 'Image Uloaded successfully');*/
    }
}
