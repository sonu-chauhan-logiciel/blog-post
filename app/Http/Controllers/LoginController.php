<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;

class LoginController extends Controller
{

	public function showLoginForm(){
		return view('login');
	}

	public function login(Request $request){
		$email = $request->input('email');
		$password = $request->input('password');
		
		if (Auth::attempt(['email' => $email, 'password' => $password, 'status' => 1], $request->filled('remember'))) {
		    $request->session()->put('email', $email);
		    return redirect()->intended('dashboard');
		}
		
		/*
		$credentials = $request->only('email', 'password');
		if (Auth::attempt($credentials) && Auth::User()->status == 'active') {
			$request->session()->put('email', $email);
			return redirect()->intended('dashboard');
        }*/
		return redirect()->route('login')->with('message', 'Invalid credentials');

	}

	public function logout(Request $request){
		Auth::logout();
		$request->session()->flash('email');
		return redirect('/login');
	}
}
