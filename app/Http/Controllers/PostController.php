<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostStoreRequest;
use App\Repositories\PostRepository;


class PostController extends Controller
{
	protected $postRepository;

	public function __construct(PostRepository $posts)
	{
	    $this->postRepository = $posts;
	}

    public function index(){
    	$posts = $this->postRepository->paginate();
    	return view('posts.postList',[
    		'posts'=>$posts
    	]);
    }

    public function create(){
    	return view('posts.createPost');
    }

    public function store(PostStoreRequest $request){
    	$this->postRepository->store($request);
    	return redirect()->route('posts.index')->with('message', 'Post created successfully');
	}

	public function show($id){
		$post = $this->postRepository->show($id);
		return view('posts.postDetail', [
			'post'=>$post
		]);
	}

	public function edit($id){
		$post = $this->postRepository->edit($id);
		return view('posts.editPost', [
			'post'=>$post
		]); 
	}

	public function update(Request $request, $id){
		$this->postRepository->update($id, $request);
		return redirect()->route('posts.index')->with('message', 'Post Updated successfully');
	}

	public function destroy($id){
		$this->postRepository->delete($id);
		return redirect()->route('posts.index')->with('message', 'Post Deleted successfully');
	}
}
