<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Repositories\UserRepository;




class ResetPasswordController extends Controller
{
    protected $usersEmail;

    public function __construct(UserRepository $email)
    {
        $this->usersEmail = $email;
    }

    public function showResetForm(Request $request, $token = null){
    	return view('resetPassword')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    public function resetPassword(Request $request){
    	$this->validate($request,[
			'token' => 'required',
            'password' => 'required|confirmed|min:8',
		]);
        $email = $this->usersEmail->getEmail($request->token);
		$password = Hash::make($request->password);
		$user = User::where('email', $email)->first();
		$user->password = $password;
		$user->setRememberToken(Str::random(60));
		$user->save();
		return redirect('/login')->with('msg', 'Password Updated');
    }
}
