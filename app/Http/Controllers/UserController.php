<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Image;
use DB;
use App\Registration;

class UserController extends Controller
{
    
    public function index(){
	    $users = DB::select('select * from registrations');
	    return view('userlist',['users'=>$users]);
    }

    public function store(Request $request)
    {
    	/*$this->validate($request,[
			'name'=>'required|min:5',
			'email'=>'required|email',
			'password'=>'required',
      	]);*/
      	$name = $request->name;
      	$email = $request->email;
      	$password = $request->password;
        if ($request->hasFile('image')) {
            $image = $request->image;
            //image name with Extension
            $filenamewithextension = $image->getClientOriginalName();
            //image name without Extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            //get file extension
            $extension = $image->getClientOriginalExtension();
            //filename to store
            $filenametostore = $filename.'_'.uniqid().'.'.$extension;
            
            Storage::put('public/images/'. $filenametostore, fopen($image, 'r+'));
            Storage::put('public/images/thumbnail/'. $filenametostore, fopen($image, 'r+'));
            $thumbnailpath = public_path('storage/images/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(100, 100)->save($thumbnailpath);
        }
        $data=array('name'=>$name,"email"=>$email,"password"=>$password,"image"=>$filenametostore);
	    DB::table('registrations')->insert($data);
	    //return redirect()->back()->with('message', 'User Registered successfully');
        //return redirect()->route('view-records');
        return redirect()->action('UserController@index')->with('message', 'User Registered successfully');
    }

    public function show($id){
        $user = Registration::find($id);
    	return view('edit',[
            'user'=>$user
        ]);
    }

    public function update(Request $request){
    	$id = $request->hidden_id;
    	$name = $request->name;
    	$email = $request->email;
    	$password = $request->password;
        $image = $request->image;
        if ($image=="") {
            $users = DB::select('select image from registrations where id = ?',[$id]);
            $image = $users[0]->image;
            DB::update('update registrations set name = ?, email = ?, password = ?, image = ? where id = ?',[$name,$email,$password,$image,$id]);
            return redirect()->action('UserController@index')->with('message', 'User Updated successfully');  
        }else{
            if ($request->hasFile('image')) {
                $image = $request->image;
                //image name with Extension
                $filenamewithextension = $image->getClientOriginalName();
                //image name without Extension
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                //get file extension
                $extension = $image->getClientOriginalExtension();
                //filename to store
                $filenametostore = $filename.'_'.uniqid().'.'.$extension;
                Storage::put('public/images/'. $filenametostore, fopen($image, 'r+'));
                Storage::put('public/images/thumbnail/'. $filenametostore, fopen($image, 'r+'));
                $thumbnailpath = public_path('storage/images/thumbnail/'.$filenametostore);
                $img = Image::make($thumbnailpath)->resize(100, 100)->save($thumbnailpath);
                DB::update('update registrations set name = ?, email = ?, password = ?, image = ? where id = ?',[$name,$email,$password,$filenametostore,$id]);
                return redirect()->action('UserController@index')->with('message', 'User Updated successfully');
            }
        }
    }
    public function delete($id){
        $users = DB::select('select image from registrations where id = ?',[$id]);
        $image = $users[0]->image;
        if(Storage::exists('public/images/'.$image) && Storage::exists('public/images/thumbnail/'.$image)) {
            if(Storage::delete('public/images/'.$image) && Storage::delete('public/images/thumbnail/'.$image)){
                DB::delete('delete from registrations where id = ?',[$id]);
                return redirect()->back()->with('message', 'User Deleted successfully');
            }else{
                dd('no');
            }
        } else {
            dd('no');
        }
    	
    }
}
