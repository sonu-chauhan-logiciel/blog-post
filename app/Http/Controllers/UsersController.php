<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserStoreRequest;
use App\Repositories\UserRepository;


class UsersController extends Controller
{
    protected $usersRepository;

    public function __construct(UserRepository $users)
    {
        $this->usersRepository = $users;
    }

    public function index(Request $request)
    {
        $users = $this->usersRepository->paginate($request);
        return view('users.user',[
            'users'=>$users
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.createuser');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        $this->usersRepository->store($request);
        return redirect()->route('user.index')->with('message', 'User Registered successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user_detail = $this->usersRepository->show($id);
        return view('users.userdetail',[
            'user'=>$user_detail
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_editing = $this->usersRepository->edit($id);
        return view('users.edit',[
            'user'=>$user_editing
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->usersRepository->update($id, $request);
        return redirect()->route('user.index')->with('message', 'User Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->usersRepository->delete($id);
        return redirect()->route('user.index')->with('message', 'User deleted successfully');
    }
}
