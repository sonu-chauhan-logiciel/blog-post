<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PostRepository;
use App\Post;
use App\Image;

class frontendController extends Controller
{
    protected $postRepository;

    public function __construct(PostRepository $posts)
    {
        $this->postRepository = $posts;
    }

    public function index(){
    	$posts = Post::with('images')->get();
    	return view('welcome',[
    		'posts'=>$posts
    	]);
    }

    public function show($id){
		$posts = Post::with('images')->find($id);
		return view('fullPost',[
			'post'=>$posts
		]);  
    }

}
