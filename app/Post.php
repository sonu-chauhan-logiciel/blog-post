<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
	use SoftDeletes;
	
    protected $fillable = [
		'user_id','post_name', 'short_description','description','status'
	];

	public function users()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	public function images(){
		return $this->hasmany('App\Image');
	}

	public function comments(){
		return $this->hasmany('App\Comment');
	}

	public function delete()
    {
        // delete all associated photos
        $this->images()->delete();

        // delete the user
        return parent::delete();
    }

    
}
