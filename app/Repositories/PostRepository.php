<?php

namespace App\Repositories;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;



class PostRepository {

	public function paginate(){
		$post = new Post;
		return $post->paginate(3);
	}
	
	public function store(Request $request){
		$id = Auth::id();
		$post = new Post;
		$post->user_id = $id;
		$post->post_name = $request->name;
		$post->short_description = $request->short_description;
		$post->description = $request->description;
		$post->status = $request->status;
		$result = $post->save();
		return $result;
	}

	public function show($id){
		$result = Post::findOrFail($id);
		return $result;
	}

	public function edit($id){
		$result = Post::find($id);
		return $result;
	}

	public function update($id, Request $request){
		$data = [
			'post_name' => $request->input('name'),
			'short_description' => $request->input('short_description'),
			'description' => $request->input('description'),
			'status' => $request->input('status')
		];
		$result = Post::where('id', $id)->update($data);
		return $result;
	}

	public function delete($id){
		$post = Post::find($id);
		$result = $post->delete($id);
		return $result;
	}
}