<?php

namespace App\Repositories;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Pagination\Paginator;
use Helper;
use DB;


class UserRepository {

	public function paginate(Request $request){
		$name = $request->input('name');
		$email = $request->input('email');
		$builder = new User;
		if ($name) {
			$builder = $builder->where('name', 'LIKE', "%$name%");	
		}
		if ($email) {
			$builder = $builder->where('email', 'LIKE', "%$email%");	
		}
		$result = $builder->sortable()->paginate(4);
		return $result;
	}
	
	public function store(Request $request){
		$password = Hash::make($request->input('password'));
		if ($request->hasFile('image')) {
			$helper = new Helper();
			$image = $helper->imageupload($request->image);
		}
		$user = new User;
		$user->name = $request->input('name');
		$user->email = $request->input('email');
		$user->password = $password;
		$user->image = $image;
		$user->status = $request->input('status');
		$result = $user->save();
		return $result;
	}

	public function show($id){
		$result = User::findOrFail($id);
		return $result;
	}

	public function edit($id){
		$result = User::find($id);
		return $result;
	}

	public function update($id, Request $request){
		$data = [
			'name' => $request->input('name'),
			'email' => $request->input('email'),
			'status' => $request->input('status')
		];
		if ($request->hasFile('image')) {
			$helper = new Helper();
			$image = $helper->imageupload($request->image);
			$data['image'] = $image;
		}
		$result = User::where('id', $id)->update($data);
		return $result;
	}

	public function delete($id){
		$imageOrgPath = config('global.user_profile_image_path.original');
		$thumbImageOrgPath = config('global.user_profile_image_path.thumbnail');
		$user = User::find($id);
		$image = $user->image;
		Storage::delete($imageOrgPath.$image);
		Storage::delete($thumbImageOrgPath.$image);
		$result = $user->delete();
		return $result;
	}

	public function getEmail($token){
		$records =  DB::table('password_resets')->get();
		foreach ($records as $record) {
		    if (Hash::check($token, $record->token) ) {
		       return $record->email;
		    }
		}
	}

}