<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <!-- Styles -->
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/tooplate_style.css') }}" rel="stylesheet">
        <script src="{{ asset('js/app.js') }}" defer></script>

        <link rel="stylesheet" href="{{ asset('css/nivo-slider.css') }}" media="screen" />
        <link rel="stylesheet" href="{{ asset('css/ddsmoothmenu.css') }}" />

    </head>
    <body style="background-image: url('images/tooplate_body.jpg');">
        <div id="tooplate_wrapper">
            <div id="tooplate_header">
                <div id="site_title">
                    <h1>
                        <a href="#" style="background-image: url('images/tooplate_logo.png');"></a>
                    </h1>
                </div>
                <div id="tooplate_menu" class="ddsmoothmenu">
                    <ul>
                        <li><a href="{{ route('login') }}">Login</a></li>
                    </ul>
                </div> <!-- end of tooplate_menu -->
            </div> <!-- END of tooplate_header -->
            @yield('content')
        </div>
        <div id="tooplate_footer_wrapper">
            <div id="tooplate_footer">
                 Copyright © 2048 Your Company Name 
            </div> <!-- END of tooplate_footer -->
        </div> <!-- END of tooplate_footer_wrapper -->  
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        
        <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/ddsmoothmenu.js') }}"></script>
        <script type="text/javascript">

        ddsmoothmenu.init({
            mainmenuid: "tooplate_menu", //menu DIV id
            orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
            classname: 'ddsmoothmenu', //class added to menu's outer DIV
            //customtheme: ["#1c5a80", "#18374a"],
            contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
        })

        </script> 
    </body>
</html>