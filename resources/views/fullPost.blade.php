@extends('frontendlayout.master')
@section('title', 'Post Detail')
@section('content')
    <div id="tooplate_main">
		<div id="content" class="float_l">
        	<div class="post">
            	
            <h2>{{ $post->post_name }}</h2>
                    
            <img src="{{ asset('blog_images/'.$post->images[0]->img_name) }}" alt="" />   
            <div class="meta">
                <span class="admin">Admin</span><span class="date">March 03, 2048</span><span class="tag"><a href="#">Freebie</a>, <a href="#">Template</a></span><span class="comment"><a href="#">5 Comments</a></span>
                <div class="cleaner"></div>
            </div> 
            
            <p>
                {{ $post->description }}
            </p>
            </div>
            
            <div id="comment_form">
                <h4>Leave your comment</h4>
                
                <form action="{{ route('comment.store', ['id' => $post->id])}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form_row">
                        <label>Your comment</label><br />
                        <textarea  name="comment" rows="5" cols="5" class="form-control"></textarea>
                        @error('comment')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <input type="submit" class="btn btn-primary" name="submit" value="Send" />
                </form>
            </div>
            <div class="cleaner h40"></div>
            <h4>Comments</h4>
                        
              	<div id="comment_section">
                    <ol class="comments first_level">
                            
                            <li>
                                <div class="comment_box commentbox1">
                                        
                                    <div class="gravatar">
                                        <img class="image_frame" src="images/avator.png" alt="author 6" />
                                    </div>
                                    
                                    <div class="comment_text">
                                        <div class="comment_author">Steven <span class="date">November 25, 2048</span> <span class="time">11:35 pm</span></div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus dictum ornare nulla ac laoreet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus dictum ornare nulla ac laoreet.</p>
                                      <div class="reply"><a href="#">Reply</a></div>
                                    </div>
                                    <div class="cleaner"></div>
                                </div>                        
                                
                            </li>
                            
                            <!--<li>
                            
                            
                                	<ol class="comments">
                                
                                        <li>
                                            <div class="comment_box commentbox2">
                                            
                                            <div class="gravatar">
                                                <img class="image_frame" src="images/avator.png" alt="author 6" />
                                            </div>
                                            <div class="comment_text">
                                            <div class="comment_author">Richard <span class="date">November 27, 2048</span> <span class="time">10:20 pm</span></div>
                                            <p>Nullam bibendum tempor est nec cursus. Sed at risus dui. Ut imperdiet libero at mauris vestibulum tempor.</p>
                                            <div class="reply"><a href="#">Reply</a></div>
                                            </div>
                                            
                                            <div class="cleaner"></div>
                                            </div>                        
                                        
                                        
                                        </li>
                                        
                                        <li>
                                        
                                
                                            <ol class="comments">
                                        
                                                <li>
                                                    <div class="comment_box commentbox1">
                                                    
                                                        <div class="gravatar">
                                        <img class="image_frame" src="images/avator.png" alt="author 6" />
                                    </div>
                                                        <div class="comment_text">
                                                            <div class="comment_author">John <span class="date">November 28, 2048</span> <span class="time">11:42 am</span></div>
                                                            <p> Vestibulum eget ligula et ipsum laoreet aliquam sed ut risus.  </p>
                                                          <div class="reply"><a href="#">Reply</a></div>
                                                        </div>
                                                        
                                                        <div class="cleaner"></div>
                                                    </div>                        
                                                    
                                                    
                                                </li>
                                        
                                            </ol>

                            
                            			</li>
                                    </ol>
                                    
    						</li>
                            
                            <li>
                                <div class="comment_box commentbox1">
                                        
                                         
                                    <div class="gravatar">
                                        <img class="image_frame" src="images/avator.png" alt="author 6" />
                                    </div>
                                    <div class="comment_text">
                                        <div class="comment_author">Martin <span class="date">November 29, 2048</span> <span class="time">07:35 am</span></div>
                                        <p> Integer semper sollicitudin quam a ornare. Nam venenatis nibh ac sem faucibus et imperdiet magna laoreet. Nulla sagittis elit in enim ullamcorper vitae tincidunt metus bibendum.</p>
                                      <div class="reply"><a href="#">Reply</a></div>
                                    </div>
                                    
                                    <div class="cleaner"></div>
                                </div>                        
                                
                                
                            </li>
                            
                            <li>
                                <div class="comment_box commentbox1">
                                        
                                   <div class="gravatar">
                                        <img class="image_frame" src="images/avator.png" alt="author 6" />
                                    </div>
                                    <div class="comment_text">
                                        <div class="comment_author">David <span class="date">November 30, 2048</span> <span class="time">10:54 am</span></div>
                                        <p> Maecenas id orci vitae lectus fermentum posuere. Quisque ut risus nibh. Etiam consequat elit eu nisi porta ac auctor nisl ultrices.</p>
                                      <div class="reply"><a href="#">Reply</a></div>
                                    </div>
                                    
                                    <div class="cleaner"></div>
                                </div>                        
                                
                                
                            </li>-->
                    </ol>
                    <div class="cleaner h20"></div>    
              		<div class="pagging">
                        <ul>
                             <li><a href="#" target="_parent">Previous</a></li>
                        <li><a href="#" target="_parent">1</a></li>
                        <li><a href="#" target="_parent">2</a></li>
                        <li><a href="#" target="_parent">3</a></li>
                        <li><a href="#" target="_parent">4</a></li>
                        <li><a href="#" target="_parent">5</a></li>
                        <li><a href="#" target="_parent">6</a></li>
                        <li><a href="#" target="_parent">Next</a></li>
                        </ul>
                    </div>    
                </div>
                
                <div class="cleaner h20"></div>
            <div class="cleaner"></div>
        </div>
        
        <div id="sidebar" class="float_r">
        	<a href="#"><img src="images/ad_300.jpg" alt="image" /></a>
            
            <div class="cleaner h40"></div>
            
            <h5>Recent Posts</h5>
            <div class="rp_pp">
                <a href="#">Integer venenatis pharetra magna vitae ultrices</a>
                <p>Feb 23, 2048 - 20 Comments</p>
                <div class="cleaner"></div>
            </div>
            <div class="rp_pp">
                <a href="#">Vestibulum quis nulla nunc, nec lobortis nunc.</a>
                <p>Feb 16, 2048 - 20 Comments</p>
                <div class="cleaner"></div>
            </div>
            <div class="rp_pp">
                <a href="#">Pellentesque convallis tristique mauris.</a>
                <p>Feb 10, 2048 - 20 Comments</p>
                <div class="cleaner"></div>
            </div>
            
            <div class="cleaner h40"></div>
            
            <h5>Popular Posts</h5>
            <div class="rp_pp">
                <a href="#">Id tempor odio faucibus et proin pharetra justo.</a>
                <p>Feb 02, 2048 - 20 Comments</p>
                <div class="cleaner"></div>
            </div>
            <div class="rp_pp">
                <a href="#">Etiam Fringilla Sapien quis mauris  vestibulum.</a>
                <p>June 03, 2048 - 20 Comments</p>
                <div class="cleaner"></div>
            </div>
            <div class="rp_pp">
                <a href="#">Sed ac arcu ipsum, ut suscipit neque. </a>
                <p>August 08, 2048 - 20 Comments</p>
                <div class="cleaner"></div>
            </div>
            
            <div class="cleaner h40"></div>
            
        	<h5>Flickr Stream</h5>
            <ul class="flickr_stream">
                <li><a href="#"><img class="image_frame" src="images/tooplate_image_02.png" alt="" /></a></li>
                <li><a href="#"><img class="image_frame" src="images/tooplate_image_03.png" alt="" /></a></li>
                <li class="no_margin_right"><a href="#"><img class="image_frame" src="images/tooplate_image_04.png" alt="" /></a></li>
                <li><a href="#"><img class="image_frame" src="images/tooplate_image_05.png" alt="" /></a></li>
                <li><a href="#"><img class="image_frame" src="images/tooplate_image_06.png" alt="" /></a></li>
                <li class="no_margin_right"><a href="#"><img class="image_frame" src="images/tooplate_image_07.png" alt="" /></a></li>
            </ul>
        </div>        
        
       
        <div class="cleaner"></div>
	</div> 
@endsection