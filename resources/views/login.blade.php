<html>
	<head>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>@yield('title')</title>
		<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/adminStyle.css') }}">
		<script src="{{ asset('js/app.js') }}" defer></script>
	</head>
	<body>
	
	<!--Login Section Start-->
	<section class="login_section">
		<div class="container">
			<div class="row">
				<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
					<div class="login_area">
						@if(session()->has('message'))
						    <div class="alert alert-danger">
						        {{ session()->get('message') }}
						    </div>
						@endif
						@if(session()->has('msg'))
						    <div class="alert alert-success">
						        {{ session()->get('msg') }}
						    </div>
						@endif
						<div class="card">
							<div class="card-body">
								<h5 class="card-title text-center" style="color: #555;">Login</h5>
								<form class="form-signin" action="{{ route('login') }}" method="POST">
									{{ csrf_field() }}
									<div class="form-group">
										<label>Email address:</label>
										<input type="email" name="email" class="form-control" placeholder="Email address" required autofocus>
									</div>

									<div class="form-group">
										<label>Password:</label>
										<input type="password" name="password" class="form-control" placeholder="Password" required>
									</div>
									<div class="form-group">
									    <div class="form-check">
									        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

									        <label class="form-check-label" for="remember">
									            {{ __('Remember Me') }}
									        </label>
									    </div>
									</div>
									<button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Login</button>
									<a class="btn btn-link" href="{{ route('password.request') }}">
									    {{ __('Forgot Your Password?') }}
									</a>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--Login Section Start-->

		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
		<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	</body>
</html>