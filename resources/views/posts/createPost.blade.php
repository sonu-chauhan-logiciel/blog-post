@extends('../layout.master')
@section('title', 'Create Post')
@section('content')

	<section class="main_section">
		@if(session()->has('message'))
		    <div class="alert alert-success">
		        {{ session()->get('message') }}
		    </div>
		@endif 
    	<div class="col-xl-12">
    		<div class="top_bar">
				<div class="left">
					<h2 class="text-info">
						Create Post
					</h2>
				</div>
				<div class="right">
					<a href="{{ route('posts.index') }}" class="btn btn-success">Back</a>
				</div>
    		</div>
    	</div>
    	<section class="form_section">
    		<div class="container">
    			<div class="row">
    				<div class="col-xl-12">
    					<div class="form_area">
    					    <form action="{{ route('posts.store') }}" method="POST" enctype="multipart/form-data">
    					        {{ csrf_field() }}
    					        <div class="row">
    					        	<div class="col-xl-6">
	    					            <div class="form-group">
	    					                <label>Post Title:</label>
	    					                <input type="text" class="form-control" name="name">
	    					                @error('name')
	    					                    <div class="text-danger">{{ $message }}</div>
	    					                @enderror
	    					            </div>
    					        	</div>
    					        </div>
    					        <div class="row">
    					        	<div class="col-xl-6">
	    					            <div class="form-group">
	    					                <label>Short Description:</label>
	    					                <input type="text" class="form-control" name="short_description">
	    					                @error('short_description')
	    					                    <div class="text-danger">{{ $message }}</div>
	    					                @enderror
	    					            </div>
    					        	</div>
    					        </div>
    					        <div class="row">
    					        	<div class="col-xl-6">
	    					            <div class="form-group">
	    					                <label>Description:</label>
	    					                <textarea class="form-control" rows="4" col="5" name="description"></textarea>
	    					                @error('description')
	    					                    <div class="text-danger">{{ $message }}</div>
	    					                @enderror
	    					            </div>
    					        	</div>
    					        </div>
    					        <div class="row">
    					        	<div class="col-xl-6">
	    					            <div class="form-group">
	    					                <label>Status:</label>
	    					                <select class="form-control" name="status">
	    					                	<option>Select the status</option>
	    					                	<option value="1">Active</option>
	    					                	<option value="0">Deactive</option>
	    					                </select>
	    					                @error('status')
	    					                    <div class="text-danger">{{ $message }}</div>
	    					                @enderror
	    					            </div>
    					        	</div>
    					        </div>
    					        <div class="row">
    					        	<div class="col-xl-6">
	    					            <div class="form-group">
	    					                <input type="submit" class="btn btn-info" name="submit" value="Add">
	    					            </div>
	    					        </div>
    					        </div>
    					    </form>
    					</div>
    				</div>
    			</div>
    		</div>
    	</section>
	</section>

@endsection