@extends('../layout.master')
@section('title', 'Post Detail')
@section('content')

	<section class="main_section">
		@if(session()->has('message'))
		    <div class="alert alert-success">
		        {{ session()->get('message') }}
		    </div>
		@endif 
    	<div class="col-xl-12">
    		<div class="top_bar">
				<div class="left">
				</div>
				<div class="right">
					<a href="{{ route('posts.index') }}" class="btn btn-success">Back</a>
				</div>
    		</div>
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Id</th>
							<th>Name</th>
							<th>Description</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>{{ $post->id }}</td>
							<td>{{ $post->post_name }}</td>
							<td>{{ $post->short_description }}</td>
							<td>{{ $post->description }}</td>
						</tr>
					</tbody>
				</table> 
			</div>
    	</div>
	</section>

@endsection