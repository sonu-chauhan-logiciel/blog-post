@extends('../layout.master')
@section('title', 'Post image')
@section('content')
    <section class="main_section">
        <div class="col-xl-12">
            <div class="top_bar">
                <div class="left">
                    <h2 class="text-info">
                        Add Image 
                    </h2>
                </div>
                <div class="right">
                    <a href="{{ route('posts.index') }}" class="btn btn-success">Back</a>
                </div>
            </div>
        </div>
        <section class="form_section">
            <!--Form Section Start-->
            <div class="form_area">
                <div class="row">
                    <div class="col-md-12">
                        <form class="row form" action="{{ route('posts.image.save', ['id' => $post->id])}}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name">Image Upload</label>
                                    <input class="form-control" type="file" name="image">
                                    @error('image')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="email">Status</label>
                                    <select name="status" class="form-control">
                                        <option value="">Select Status</option>
                                        <option value="1">Enable</option>
                                        <option value="0">Disable</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" style="margin-top:30px;">
                                    <input type="submit" class="btn btn-primary" value="Submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--Form Section End-->


            <!--VIew Section Start-->
            <div class="view_area">
                <table class="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Image</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <a href="" class="btn btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                                <a href="" class="btn btn-info">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!--VIew Section End-->
        </section>
    </section>
@endsection