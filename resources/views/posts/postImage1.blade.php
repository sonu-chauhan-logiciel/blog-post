@extends('../layout.master')
@section('title', 'Post image')
@section('content')
    <section class="main_section">
        <div class="col-xl-12">
            <div class="top_bar">
                <div class="left">
                    <h2 class="text-info">
                        Add Image 
                    </h2>
                </div>
                <div class="right">
                    <a href="{{ route('posts.index') }}" class="btn btn-success">Back</a>
                </div>
            </div>
        </div>
        <section class="form_section">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="form_area">
                            <!--<div class="row">
                                <div class="col-xl-6">
                                    <div class="form-group">
                                        <label>Post Title:</label>
                                        <input type="text" class="form-control" name="name" value="{{ $post->post_name }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-6">
                                    <div class="form-group">
                                        <label>Short Description:</label>
                                        <input type="text" class="form-control" name="short_description" value="{{ $post->short_description }}">
                                        @error('short_description')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>-->
                            <div class="row">
                                <div class="col-xl-6">
                                    <label>Image:</label>
                                    <form method="post" action="{{ route('posts.image.save', ['id' => $post->id]) }}" enctype="multipart/form-data"
                                          class="dropzone" id="dropzone">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-6">
                                    <div class="form-group">
                                        <a class="btn btn-info" href="" onclick="event.preventDefault(); document.getElementById('dropzone').submit();">Add Image</a> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection