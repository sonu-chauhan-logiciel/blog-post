@extends('../layout.master')
@section('title', 'Registration Form')
@section('content')
	<section class="header_section">
	    <div class="container">
	        <div class="row">
	            <div class="col-xl-12">
	                <div class="header">
	                    <h4>
	                        Registration Form
	                    </h4>
	                    <a href="{{ route('user.index')}}" class="btn btn-success">Back</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
	<section class="form_section">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="form_area">
					    <form action="{{ route('user.store')}}" method="POST" enctype="multipart/form-data">
					        {{ csrf_field() }}
					        <div class="row">
					            <div class="form-group">
					                <label>Name:</label>
					                <input type="text" class="form-control" name="name">
					                @error('name')
					                    <div class="text-danger">{{ $message }}</div>
					                @enderror
					            </div>
					        </div>
					        <div class="row">
					            <div class="form-group">
					                <label>Email:</label>
					                <input type="email" class="form-control" name="email">
					                @error('email')
					                    <div class="text-danger">{{ $message }}</div>
					                @enderror
					            </div>
					        </div>
					        <div class="row">
					            <div class="form-group">
					                <label>password:</label>
					                <input type="password" class="form-control" name="password">
					                @error('password')
					                    <div class="text-danger">{{ $message }}</div>
					                @enderror
					            </div>
					        </div>
					        <div class="row">
					            <div class="form-group">
					                <label>Status:</label>
					                <select class="form-control" name="status">
					                	<option>Select the status</option>
					                	<option value="1">Active</option>
					                	<option value="0">Deactive</option>
					                </select>
					                @error('status')
					                    <div class="text-danger">{{ $message }}</div>
					                @enderror
					            </div>
					        </div>
					        <div class="row">
					            <div class="form-group">
					                <label>Image:</label>
					                <input type="file" class="form-control" name="image">
					                @error('image')
					                    <div class="text-danger">{{ $message }}</div>
					                @enderror
					            </div>
					        </div>
					        <div class="row">
					            <div class="form-group">
					                <input type="submit" class="btn btn-info" name="submit" value="Add">
					            </div>
					        </div>
					    </form>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection