@extends('../layout.master')
@section('title', 'User Update')
@section('content')
	<section class="header_section">
	    <div class="container">
	        <div class="row">
	            <div class="col-xl-12">
	                <div class="header">
	                    <h4>
	                        Update User
	                    </h4>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
	<section class="form_section">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="form_area">
					    <form action="{{ route('user.update', ['id' => $user->id]) }}" method="POST" enctype="multipart/form-data">
					    	{{method_field('PUT')}}
					        {{ csrf_field() }}
					        <div class="row">
					            <div class="form-group">
					                <label>Name:</label>
					                <input type="text" class="form-control" name="name" value="{{$user->name}}">
					            </div>
					        </div>
					        <div class="row">
					            <div class="form-group">
					                <label>Email:</label>
					                <input type="email" class="form-control" name="email" value="{{$user->email}}">
					            </div>
					        </div>
					        <div class="row">
					            <div class="form-group">
					                <label>Status:</label>
					                <select class="form-control" name="status">
					                	@php
					                		$value = $user->status;
					                	@endphp
					                	<option value="1"
					                		@php
					                			if($value == 1){
					                				echo 'selected';
					                			}
											@endphp
					                	>Active</option>
					                	<option value="0"
            		                		@php 
            		                			if($value == 0){
            		                				echo 'selected';
            		                			}
            								@endphp
					                	>Deactive</option>
					                </select>
					            </div>
					        </div>
					        <div class="row">
					            <div class="form-group">
					                <label>Image:</label>
					                <input type="file" class="form-control" name="image">
					                <img src="{{ asset('storage/images/').'/'.$user->image}}" alt="..." height="50" width="50" />
					            </div>
					        </div>
					        <div class="row">
					            <div class="form-group">
					                <input type="submit" class="btn btn-info" name="submit" value="Update">
					            </div>
					        </div>
					    </form>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection