@extends('../layout.master')
@section('title', 'Laravel-Crud')
@section('content')
	<section class="main_section">
		@if(session()->has('message'))
		    <div class="alert alert-success">
		        {{ session()->get('message') }}
		    </div>
		@endif 
    	<div class="col-xl-12">
    		<div class="top_bar">
				<div class="left">
					<form action="{{ route('user.index') }}" method="GET" role="search">
						<input type="text" class="form-control" placeholder="Enter Name" name="name" value="{{ Request::input('name') }}">
						<input type="text" class="form-control" placeholder="Enter Email" name="email" value="{{ Request::input('email') }}">
						<div class="input-group">
							&nbsp;
							<input type="submit" value="Search" class="btn btn-info">
							&nbsp;
							<a href="{{ route('user.index') }}" class="btn btn-success">Reset</a>
						</div>
					</form>
				</div>
				<div class="right">
					<a href="{{ route('user.create') }}" class="btn btn-info">Add User</a>
					<a href="{{ route('user.index') }}" class="btn btn-success">Back</a>
				</div>
    		</div>
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>@sortablelink('id', 'ID')</th>
							<th>@sortablelink('name', 'Name')</th>
							<th>@sortablelink('email', 'Email')</th>
							<th>Image</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@if(count($users) > 0)
							@foreach ($users as $user)
								<tr>
									<td>{{ $user->id }}</td>
									<td>{{ $user->name }}</td>
									<td>{{ $user->email }}</td>
									<td>
										<img src="{{ asset('storage/images').'/' .$user->image }}" alt="..." height="30" width="30" />
									</td>
									<td>
										<a href="{{ route('user.edit', ['id' => $user->id]) }}" class="btn btn-info">
											<i class="fa fa-pencil"></i>
										</a>
										<a href="{{ route('user.view', ['id' => $user->id]) }}" class="btn btn-primary">
											<i class="fa fa-eye"></i>
										</a>
										<form method="POST" action="{{ route('user.delete', ['id' => $user->id]) }}">
										    {{ csrf_field() }}
										    {{ method_field('DELETE') }}
										    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
										</form>
									</td>
								</tr>
							@endforeach
						@else
							<tr>
								<td colspan="5">{{ 'No Record Found ! Try Again!!' }}</td>
							</tr>
						@endif
					</tbody>
				</table> 
				{!! $users->appends(request()->except('page'))->render() !!}
			</div>
    	</div>
	</section>
@endsection