@extends('../layout.master')
@section('title', 'Laravel-Crud')
@section('content')
	<section class="main_section">
		<div class="container">
		    <div class="row">
		    	<div class="col-xl-12">
		    		<div class="top_bar">
						<div class="left">
						</div>
						<div class="right">
							<a href="{{ route('user.index') }}" class="btn btn-success">Back</a>
						</div>
		    		</div>
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Sr no.</th>
									<th>Name</th>
									<th>Email</th>
									<th>Image</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>{{$user->id}}</td>
									<td>{{$user->name}}</td>
									<td>{{$user->email}}</td>
									<td>
										<img src="{{ asset('storage/images').'/' .$user->image }}" alt="..." height="50" width="50" />
									</td>
								</tr>
							</tbody>
						</table>  
					</div>
		    	</div>
			</div>
		</div>
	</section>
@endsection