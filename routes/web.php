<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/fullpost', function () {
    return view('fullPost');
});

Route::get('/', 'frontendController@index');
Route::get('/post/{id}', 'frontendController@show')->name('postdetail');
Route::post('/comment/{id}', 'CommentController@store')->name('comment.store');



Route::prefix('users')->name('user.')->group(function () {
	Route::get('/', 'UsersController@index')->name('index');
    Route::get('/add', 'UsersController@create')->name('create');
    Route::post('/', 'UsersController@store')->name('store');
    Route::get('/edit/{id}', 'UsersController@edit')->name('edit');
    Route::put('/{id}', 'UsersController@update')->name('update');
    Route::get('/view/{id}', 'UsersController@show')->name('view');
    Route::delete('/delete/{id}','UsersController@destroy')->name('delete');
});

Route::prefix('posts')->name('posts.')->group(function () {
    Route::get('/', 'PostController@index')->name('index');
    Route::get('/addpost', 'PostController@create')->name('create');
    Route::post('/', 'PostController@store')->name('store');
    Route::get('/edit/{id}', 'PostController@edit')->name('edit');
    Route::put('/{id}', 'PostController@update')->name('update');
    Route::get('/view/{id}', 'PostController@show')->name('view');
    Route::delete('/delete/{id}','PostController@destroy')->name('delete');
    Route::get('/image/{id}', 'ImageController@index')->name('image');
    Route::post('/image/{id}', 'ImageController@store')->name('image.save');

});

Route::get('/login', 'LoginController@showLoginForm')->name('login');
Route::post('/login', 'LoginController@login');
Route::post('/logout', 'LoginController@logout')->name('logout');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', 'DashboardController@index');
});

Route::get('/password/reset', 'ForgotPasswordController@showForm')->name('password.request');
Route::post('password/email', 'ForgotPasswordController@sendEmail')->name('password.email');
Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'ResetPasswordController@resetPassword')->name('password.update');

/*Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');*/
